﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorObject : MonoBehaviour
{
    void Awake()
    {
        Color[] objectsColor = { Color.green, Color.red, Color.yellow, Color.blue };
        gameObject.GetComponent<Renderer>().material.color = objectsColor[Random.Range(0, objectsColor.Length)];
    }
}
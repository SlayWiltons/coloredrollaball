﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.R))
        {
            RestartTheGame();
        }
    }

    void RestartTheGame()
    {
        SceneManager.LoadScene(0);
        Time.timeScale = 1;
    }    
}
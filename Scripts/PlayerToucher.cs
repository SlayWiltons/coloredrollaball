﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerToucher : MonoBehaviour
{
    public List<Color> colorsOfObjects;
    public List<GameObject> allPickUps;
    public TextMeshProUGUI countText;
    public TextMeshProUGUI statusText;
    public TextMeshProUGUI timeText;
    private int pickUps;
    private int count;
    private float timeSeconds;    
    private bool isTicking;

    void Start()
    {
        count = 0;
        SetCountText();
        statusText.text = "";
        pickUps = GameObject.FindGameObjectsWithTag("PickUp").Length;        
        allPickUps.AddRange(GameObject.FindGameObjectsWithTag("PickUp"));        
        timeSeconds = 15f;
        isTicking = false;
        SetPlayerColor();
        Time.timeScale = 1;
    }    

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
            if (other.GetComponent<Renderer>().material.color == gameObject.GetComponent<Renderer>().material.color)
            {
                timeSeconds += 3f;
            }
            else
            {
                timeSeconds -= 3f;
            }
            if (allPickUps.Count == 1)
            {
                allPickUps.Clear();
            }
            else
            {
                allPickUps.Remove(other.gameObject);
                SetPlayerColor();
            }
            Destroy(other.gameObject);
            count++;
            SetCountText();
        }
    }

    void Update()
    {
        timeText.text = timeSeconds.ToString();
        if (isTicking == false)
        {
            StartCoroutine(Ticking());
        }
        if (timeSeconds <= 3f)
        {
            timeText.color = Color.red;
        }
        else
        {
            timeText.color = Color.black;
        }

        if (timeSeconds < 0f)
        {
            timeSeconds = 0f;
        }
        if (timeSeconds == 0 && allPickUps.Count > 0)
        {
            SetStatusText("You lose :(");
        }
        if (count >= pickUps)
        {
            SetStatusText("You win!!!");
        }
    }

    private IEnumerator Ticking()
    {
        isTicking = true;
        yield return new WaitForSeconds(1f);
        timeSeconds -= 1f;
        if (timeSeconds < 0)
        {
            timeSeconds = 0f;
        }
        isTicking = false;
    }

    private void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
    }

    private void SetStatusText(string _statusText)
    {        
        statusText.text = _statusText;        
        Time.timeScale = 0;
        timeText.text = "";
    }

    private void SetPlayerColor()
    {
        colorsOfObjects.Clear();
        for (int i = 0; i < allPickUps.Count; i++)
        {
            colorsOfObjects.Add(allPickUps[i].GetComponent<Renderer>().material.color);
        }
        gameObject.GetComponent<Renderer>().material.color = colorsOfObjects[Random.Range(0, colorsOfObjects.Count)];
    }
}
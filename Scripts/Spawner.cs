﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject pickUpObject;
    public GameObject[] spawners;

    private void Awake()
    {
        spawners = GameObject.FindGameObjectsWithTag("Spawner");
        for (int i = 0; i < spawners.Length; i++)
        {
            Instantiate(pickUpObject, new Vector3(spawners[i].transform.position.x, spawners[i].transform.position.y, spawners[i].transform.position.z), Quaternion.identity);
        }
    }
}